import XCTest

import KidChessTests

var tests = [XCTestCaseEntry]()
tests += KidChessTests.allTests()
XCTMain(tests)
