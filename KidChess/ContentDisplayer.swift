//
//  ContentDisplayer.swift
//  KidChess
//
//  Created by Ilya Belegradek on 5/27/19.
//  Copyright © 2019 kidchess. All rights reserved.
//

import UIKit
import WebKit

//This class displays the content the user wished to view by loading it from the website
//It includes a label which shows what content the user is looking at
//and a button to return back to the main menu

class ContentDisplayer: UIViewController
{
    //
    //variables
    //
    
    //used by ViewController.swift to tell this class what to load
    var content = ""
    
    //used to load the webView
    var url = ""
    
    
    var contentWebView =  WKWebView()
    
    
    
    //
    //Outlets
    //
    
    //the title of the activity
    @IBOutlet weak var activityTitle: UINavigationItem!
    
    
    

    
    
    //
    //methods
    //
    
    
    //initial loading
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(named: "BackgroundColor")
                
        print("wtf")
        
        //initializing webview
        let config = WKWebViewConfiguration()
        let contentController = WKUserContentController()
        
        //used to disable/enable scrolling when user is dragging a piece
        //see extension below
        contentController.add(self, name: "enableScroll")
        contentController.add(self, name: "disableScroll")
        config.userContentController = contentController
        contentWebView = WKWebView(frame: self.view.bounds, configuration: config)
        self.view.addSubview(contentWebView)
        
        
        activityTitle.title = content
        
        if(content == "EZChess") //loading EZChess
        {
            let url = URL(string: "file:///Users/ilya/kidchess_heroku_checkmate/dist/ezchess/index.html")!
            //let url = URL(string: "https://kidchess.herokuapp.com/dist/ezchess/")!
            //let url = URL(string: "https://www.kidchess.com/ezchess/index.html")!
            contentWebView.load(URLRequest(url: url))
        }
        else if(content == "Monster Chess") //loading Monster Chess
        {
            let url = URL(string: "file:///Users/ilya/kidchess_heroku_checkmate/dist/monster_chess/index.html")!
            //let url = URL(string: "https://kidchess.herokuapp.com/dist/monster_chess/")!
            //let url = URL(string: "https://www.kidchess.com/monsterchess/index_for_iframe.html")!
            contentWebView.load(URLRequest(url: url))
        }
        else if(content == "Checkmate the King") //loading Checkmate the King
        {
            let url = URL(string: "file:///Users/ilya/kidchess_heroku_checkmate/dist/checkmate_the_king/index.html")!
            //let url = URL(string: "https://kidchess.herokuapp.com/dist/checkmate_the_king/")!
            //let url = URL(string: "https://www.kidchess.com/checkmate/index_for_iframe.html")!
            contentWebView.load(URLRequest(url: url))
        }
    }
    

    //action that occurrs when the user presses the "Back <" button
    //takes the user back to the main menu
    @IBAction func backAction(_ sender: Any)
    {
        self.performSegue(withIdentifier: "mainMenuSegue", sender: self)
    }
}

//
//message handler
//used to disable/enable scrolling when the user is dragging/dropping a piece
//
extension ContentDisplayer: WKScriptMessageHandler
{
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage)
    {
        //user dropped the piece, so they should be able to scroll the screen
        if(message.name == "enableScroll")
        {
            contentWebView.scrollView.isScrollEnabled = true
        }
        else if(message.name == "disableScroll") //user is dragging the piece, so the screen shouldn't scroll
        {
            contentWebView.scrollView.isScrollEnabled = false
        }
    }
}
