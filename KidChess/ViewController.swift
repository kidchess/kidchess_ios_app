//
//  ViewController.swift
//  KidChess
//
//  Created by Ilya Belegradek on 5/27/19.
//  Copyright © 2019 kidchess. All rights reserved.
//

import UIKit

//This class represents the main menu of the app
//From here, the user can select an activity to do in the app
//and navigate to it
//This class contains a scrollable list of activities to choose from
//as well as a hamburger menu which the user can also use to navigate

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    
    
    
    //
    //outlets
    //
    
    //a scrollable view that contains all the activities the user can play
    //they can click on one of them to play it
    @IBOutlet weak var activityTable: UITableView!
    
    //contains the different actions in the hamburger menu
    @IBOutlet weak var hamburgerActions: UIStackView!
    
    //how close the table view is to the left side of the screen
    //used to slide it over when you open the hamburger menu
    @IBOutlet weak var tableLeadingConstraint: NSLayoutConstraint!
    
    //how close the table view is to the top of the screen
    //used to determine where to place the view that prevents interaction with the rest of the screen
    //when the hamburger menu is open
    @IBOutlet weak var tableTopConstraint: NSLayoutConstraint!
    
    //the navigation bar at the top of the screen
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    
    //
    //variables
    //
    
    //the different activities kids can do in the app
    //extra spaces to make the descriptions the proper width, ideally need a better method
    let activites = ["EZChess", "Monster Chess", "Checkmate the King", "Flash Chess", "Knight's Tour", "Evaluator"]
    
    //stores images for the different activities
    let activityImages = ["ezchessImage.jpg", "monsterchessImage.jpg", "checkmateImage.jpg", "flashchessImage.jpg", "knightstourImage.jpg", "ezchessImage.jpg"]
    
    //images that should be used for iPads?
    let bigImages = ["bigEZChess", "bigMonsterChess", "bigCheckmateScreenshot", "bigFlashChess", "bigKnightsTour", "bigEvaluator"]
    
    //the descriptions of what the different activities actually entail
    let activityDescriptions = ["Practice piece movement by playing a real game of chess against our EZChess computer!",
    "Learn how the pieces move by playing Monster Chess. Try and beat your own best time!",
    "Practice different forms of check-mate now. Try and beat your own best time!",
    "Play at three different levels: Beginner, Casual, and Advanced.",
    "Visit each square on the chessboard one time only with just a lone knight. How tough can it be?",
    "Quiz your chess skills in different areas such as tactics and checkmates."]
    
    
    
    //used to determine how to navigate
    //fromTable determines whether you're navigating from the tableview or the hamburger menu
    //the other booleans are used when navigating from the hamburger menu
    var fromTable = true
    var ezChess = false
    var monsterChess = false
    var checkmateKing = false
    var flashChess = false
    var knightsTour = false
    var evaluator = false
    
    
    //used to prevent interaction with the main menu when the hamburger menu is open
    let darkView = UIView()
    
    //line that separates the hamburger menu from the table view
    //makes it look nicer
    let lineView = UIView()
    
    //determines the sceen size
    //if larger/smaller screen that normal, need to use different dimensions
    var largeScreen = false
    var smallScreen = false
    
    
    //
    //methods
    //
    

    
    //This function performs initial setup for the main menu
    override func viewDidLoad()
    {
        super.viewDidLoad()

        //making sure all booleans are the values they're supposed to be
        monsterChess = false
        ezChess = false
        checkmateKing = false
        flashChess = false
        knightsTour = false
        evaluator = false
        fromTable = true

        //determining the screen size
        if(UIScreen.main.bounds.width > 430)
        {
            largeScreen = true
        }
        
        //needed to make the tableview work
        activityTable.dataSource = self
        activityTable.delegate = self
        
        //setting the screen's background color
        activityTable.backgroundColor = UIColor(named: "BackgroundColor")
        self.view.backgroundColor = UIColor(named:"BackgroundColor")
    }
    
    
    
    
    //
    //table view functions
    //based on this tutorial
    //http://www.thomashanning.com/uitableview-tutorial-for-beginners/
    //
    
    
    //set the contents of the table view cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //cell initialization
        let cell = tableView.dequeueReusableCell(withIdentifier: "activityIdentifier") as! ActivityCell
        
        //get appropriate activity title, image, and description
        let text =  activites[indexPath.row]
        let description = activityDescriptions[indexPath.row]
        
        
        var image = ""
        if(largeScreen)
        {
            image = bigImages[indexPath.row]
            cell.imageWidthConstraint.constant = 444
            //cell.removeConstraint(cell.imageWidthConstraint)
        }
        else
        {
            image = activityImages[indexPath.row]
        }
        
  
        //making the title larger/bold to make it look better
        //how large/bold depends on the screen size
        //combining title and description

        var fontAttribute = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 17)]
        var fontAttribute2 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)]
        
        if(largeScreen)
        {
            fontAttribute = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 30)]
            fontAttribute2 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)]
        }
        else if(smallScreen)
        {
           
        }
        
        
        let attributedTitle = NSMutableAttributedString(string: text, attributes: fontAttribute)
        
        //have to convert to attributed string to concatenate
        let attributedNewLine = NSMutableAttributedString(string: "\n")
        let attributedDescription = NSMutableAttributedString(string: description, attributes: fontAttribute2)
        attributedTitle.append(attributedNewLine)
        attributedTitle.append(attributedDescription)
       
        
        //setting the cell's text/description and image
        cell.activityLabel.attributedText = attributedTitle
        cell.activityImage.image = UIImage(named: image)
     
        
        return cell
    }
    
    //determines the number of sections in the tableView
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    //determines the number of rows in the tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return activites.count
    }

    
    //changes the height of the table view cells
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(largeScreen) //making cells larger for iPads
        {
            return 460
        }
        else
        {
            return 180
        }
    }
    
    
    
    
    //
    //hamburger menu functions
    //
    
    //actions that occurs when you press the hamburger menu button
    //either shows or hides the hmaburger menu, depending on whether it's currently hidden or shown
    @IBAction func hamburgerMenuAction(_ sender: Any)
    {
        if(hamburgerActions.isHidden)
        {
            showHamburger()
        }
        else
        {
            hideHamburger()
        }
    }
    
    //shows the hamburger menu
    //needs to unhide it, slide over the table view, and create a new view that obscures the table view
    //to let users tap somewhere to close hamburger menu and to prevent them from interacting with table view
    func showHamburger()
    {
        hamburgerActions.isHidden = false
        fromTable = false
        tableLeadingConstraint.constant += self.hamburgerActions.frame.width + 20 //slides table view over
        
        //create the view that obscures the table view
        //give it a background color to make the screen "grayed out"
        //determing the positioning based on the positioning of the table view
        //adding a gesture recognizer that closes the hamburger menu when you tap outside of it
        //adding the view to the screen
        
        //darkView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        darkView.frame = CGRect(x:tableLeadingConstraint.constant, y:activityTable.frame.minY - tableTopConstraint.constant, width: activityTable.frame.width, height: activityTable.frame.height + tableTopConstraint.constant)
        let darkViewTap = UITapGestureRecognizer(target: self, action: #selector(hideHamburger))
        darkView.addGestureRecognizer(darkViewTap)
        self.view.addSubview(darkView)
        
        //creating a vertical line between the hamburger menu and the table view to make it look nicer
        lineView.frame = CGRect(x:tableLeadingConstraint.constant - 15, y:0, width: 1, height: self.view.frame.height)
        lineView.backgroundColor = UIColor.black
        self.view.addSubview(lineView)
        
    }
    
    //hides the hamburger menu
    //reverses everything that showHamburger() did
    @objc func hideHamburger()
    {
        hamburgerActions.isHidden = true
        fromTable = true
        tableLeadingConstraint.constant -= hamburgerActions.frame.width + 20
        lineView.removeFromSuperview()
        darkView.removeFromSuperview()
    }
    
    
    
    
    //
    //navigation functions
    //
    
    
    
    //function for the EZChess button in the hamburger menu
    //navigates to the content displayer and tells it that the user picked EZChess
    @IBAction func EZChessSegue(_ sender: Any)
    {
        ezChess = true
        self.performSegue(withIdentifier: "contentDisplayerSegue", sender: self)
    }
    
    //function for the monster chess button in the hamburger menu
    //navigates to the content displayer and tells it that the user picked monster chess
    @IBAction func MonsterChessSegue(_ sender: Any)
    {
        monsterChess = true
        self.performSegue(withIdentifier: "contentDisplayerSegue", sender: self)
    }
    
    
    //function for the checkmate the king button in the hamburger menu
    //navigates to the content displayer and tells it that the user picked checkmate the king
    @IBAction func CheckmateKingSegue(_ sender: Any)
    {
        checkmateKing = true
        self.performSegue(withIdentifier: "contentDisplayerSegue", sender: self)
    }
    
    //function for the flash chess button in the hamburger menu
    @IBAction func FlashChessSegue(_ sender: Any)
    {
        flashChess = true
        self.performSegue(withIdentifier: "contentDisplayerSegue", sender: self)
    }
    
    //function for the knight's tour button in the hamburger menu
    @IBAction func KnightsTourSegue(_ sender: Any)
    {
        knightsTour = true
        self.performSegue(withIdentifier: "contentDisplayerSegue", sender: self)
    }
    
    //function for the evaluator button in the hamburger menu
    @IBAction func EvaluatorSegue(_ sender: Any)
    {
        evaluator = true
        self.performSegue(withIdentifier: "contentDisplayerSegue", sender: self)
    }
    
    //Used to tell the ContentDisplayer which content to load
    //either gets the game chosen from the tableview's indexPath property (which shows which row was selected)
    //or from one of the booleans that's set to true when the user taps the appropriate button in the hamburger menu
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if(fromTable) //if navigating from the table, use the selected row to determine which game to load
        {
            let contentVC = segue.destination as! ContentDisplayer
            let indexPath = activityTable.indexPathForSelectedRow //get which row the user selected
            contentVC.content = activites[(indexPath?.row)!]
        }
        else //use the selected button from the hamburger menu to determine which game to load
        {
            let contentVC = segue.destination as! ContentDisplayer
            if(ezChess)
            {
                contentVC.content = "EZChess"
            }
            else if(monsterChess)
            {
                contentVC.content = "Monster Chess"
            }
            else if(checkmateKing)
            {
                contentVC.content = "Checkmate the King"
            }
            else if(flashChess)
            {
                contentVC.content = "Flash Chess"
            }
            else if(knightsTour)
            {
                contentVC.content = "Knight's Tour"
            }
            else if(evaluator)
            {
                contentVC.content = "Evaluator"
            }
        }
    }
}

