//
//  ActivityCell.swift
//  KidChess
//
//  Created by Ilya Belegradek on 5/28/19.
//  Copyright © 2019 kidchess. All rights reserved.
//

import UIKit

//This class is used in the main menu to represent a cell in the activity table view
//each cell contains the name of the activity and a corresponding image

class ActivityCell: UITableViewCell
{
    //
    //outlets
    //
    
    //the image associated with the activity
    @IBOutlet weak var activityImage: UIImageView!
    
    //contains the name and description of the activity
    @IBOutlet weak var activityLabel: UILabel!
    
    //how wide the image is allowed to be
    //changes when you use a larger/smaller screen
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    
    //
    //methods
    //
    
    //initializition function
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.backgroundColor = UIColor(named: "BackgroundColor")
    }

    //function that determines behavior when the cell is selected
    //no custom behavior 
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}
